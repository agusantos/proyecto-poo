#ifndef Socios_H
#define Socios_H
#include "Persona.h"

#include <vector>
using namespace std;

class Socios{
	Persona v;
	
public:
	vector<Persona>p;
	void LeerArchivoSocios();
	void AgregarPersona();
	void EliminarElemento(int borrar);
	void ActualizarContacto(int cual);
	void CargarArchivoAVector();
	void BuscarNombre(string n);
	void PruebaVector();
	int CantidadDeSocios();
	
	string DevolverNombre(int n);
	
	int DevolverNumeroSocio(int dni);
};
#endif
