#ifndef PAGO_H
#define PAGO_H
#include <iostream>
#include <vector>
#include "Socios.h"
#include <ctime>
#include <tuple>
using namespace std;

struct UltimoPago{
	int s_socio,s_plan,ult_dia,ult_mes,ult_anio,s_diferencia;
};

class Pagos : public Socios{
	vector<UltimoPago>v;
public:
	Pagos();
	void Pagar(int socio, int plan);	
	tuple<int,int,int> Horario();
	void ConsultarUltimoPago(int socio);
	int ConsultarPlan(int socio);
	int ConsultarDiferencia(int socio);
	void CargarVectorPago();
	void BorrarPagoSocio(int socio);
	void Prueba();
	
	int CalcularDiferencia(int socio);
	
	
	int DevolverSocio(int dni);
	string DevolverNombreIngreso(int n);
};

#endif

