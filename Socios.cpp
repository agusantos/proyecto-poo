#include "Socios.h"
#include <fstream>
#include <vector>
#include <iostream>
#include <string>


using namespace std;


void Socios::LeerArchivoSocios(){
	ifstream archi2("ListaSocios.txt", ios::in); 
	if (archi2.is_open()){	
		string auxnom,auxap,auxdirec, auxlong,auxemail;
		int auxdia,auxmes,auxanio,auxdni;
		Persona aux; int i=1;
		while (getline(archi2,auxnom)  and  archi2 >> auxap >>auxlong  
			   and archi2.ignore() and getline(archi2,auxdirec) and archi2>>auxemail and archi2.ignore()
			   and archi2 >> auxdni >> auxdia >> auxmes >> auxanio and archi2.ignore()){
			
			cout << "Contacto " << i <<":"<<endl;
			cout<< "Nombre: " << auxnom << " " << auxap << endl;
			cout << "  Telefono: " << auxlong << endl;
			cout << "  Direccion: " << auxdirec << endl;
			cout << "  Correo electronico: " << auxemail << endl;
			cout << "  Numero de documento: " << auxdni << endl;
			cout << "  Fecha de nacimiento: " << auxdia << "/" << auxmes << "/" << auxanio << endl;
			i++;
		}
	}
	archi2.close();
}



void Socios::AgregarPersona(){
	string n,ap,direc,correo;
	char a;
	long long t;
	int d,m,an; int _dni;
	Persona y;
	ofstream archi("ListaSocios.txt",ios::app);
	if (archi.is_open()){
		do {
			cout << "Ingrese el nombre: "; cin.ignore(); getline(cin,n) ; y.AsignarNombre(n);
			cout << "Ingrese el apellido: "; cin >> ap ; y.AsignarApellido(ap); 
			cout << "Ingrese el telefono: "; cin >> t; y.AsignarTelefono(t);
			cout << "Ingrese la direccion: ";cin.ignore(); getline(cin,direc); y.AsignarDireccion(direc);
			cout << "Ingrese correo electronico: "; cin >> correo; y.AsignarEmail(correo);
			cout << "Ingrese el numero de documento:"; cin >> _dni; y.AsignarDni(_dni);
			cout << "Ingrese fecha de nacimiento: " << endl; cout << "Dia: "; cin >> d;
			cout << "Mes: "; cin >> m;
			cout << "Anio: "; cin >> an;
			y.AsignarFechaNac(d,m,an);
			archi << y.VerNombre() << endl;
			archi << y.VerApellido() << endl;
			archi << y.VerTelefono() << endl;
			archi << y.VerDireccion() << endl;
			archi << y.VerEmail() << endl;
			archi << y.VerDni() << endl;
			archi << y.VerDiaNac() << endl; archi << y.VerMesNac() << endl; archi << y.VerAnioNac() << endl;
			cout << "¿Desea agregar otro contacto?: (Ingrese S para confirmar) " ; cin >> a;
		} while (a == 'S'||a == 's') ;
	}	
}

void Socios::CargarArchivoAVector(){
	p.erase(p.begin(),p.end());
	ifstream file2("ListaSocios.txt");
	string _n,_ap,_direc,_email; long long _p; int _d,_m,_y,documento; 
	Persona aux;
	while( getline(file2,_n) and  file2 >> _ap >> _p
		  and file2.ignore() and getline(file2,_direc) and file2>>_email and file2.ignore()
		  and file2 >> documento >> _d >> _m >> _y and file2.ignore()){
		aux.AsignarNombre(_n); aux.AsignarApellido(_ap); aux.AsignarTelefono(_p); aux.AsignarDireccion(_direc);
		aux.AsignarEmail(_email); aux.AsignarDni(documento); aux.AsignarFechaNac(_d, _m, _y);
		p.push_back(aux);
	}
		  
		  //	
		  //	Persona aux; 
		  //	for(size_t i=0;i<h.size();i++) { 
		  //		_n = h[i].name;
		  //		_ap = h[i].lastname;
		  //		_p = h[i].phone;
		  //		_direc = h[i].address;
		  //		_email = h[i].email;
		  //		documento = h[i].dni_;
		  //		_d = h[i].day; _m = h[i].month; _y = h[i].year;
		  //		aux.AsignarNombre(_n); aux.AsignarApellido(_ap); aux.AsignarTelefono(_p); aux.AsignarDireccion(_direc);
		  //		aux.AsignarEmail(_email); aux.AsignarDni(documento); aux.AsignarFechaNac(_d, _m, _y);
		  //		p.push_back(aux);
		  //	}
}
void Socios::EliminarElemento(int borrar){

	if(borrar > p.size()){ cout << "No se encuentra registro de ese socio en la base de datos. " << endl;}
	else{
		CargarArchivoAVector();
		p.erase(p.begin()+borrar);
		ofstream archi3("ListaSocios.txt",ios::trunc);
		for(size_t i=0;i<p.size();i++) { 
			archi3 << p[i].VerNombre() << endl;
			archi3 << p[i].VerApellido() << endl;
			archi3 << p[i].VerTelefono() << endl;
			archi3 << p[i].VerDireccion() << endl;
			archi3 << p[i].VerEmail() << endl;
			archi3 << p[i].VerDni() << endl;
			archi3 << p[i].VerDiaNac() << endl; archi3 << p[i].VerMesNac() << endl; archi3 << p[i].VerAnioNac() << endl;
		}
		cout << "Elemento numero " << borrar << " eliminado." << endl;
		
		archi3.close();	}
}

void Socios::ActualizarContacto(int cual){
	CargarArchivoAVector();
	string act_nom,act_ap,act_direc,act_em;
	long long act_tel;
	int act_d,act_m,act_a,act_dni;
	cout << "Ingrese el nuevo nombre: "; cin.ignore();getline(cin,act_nom);
	cout << "Ingrese el nuevo apellido: "; cin >> act_ap;
	cout << "Ingrese el nuevo telefono: "; cin >> act_tel;
	cout << "Ingrese la nueva direccion: "; cin.ignore(); getline(cin,act_direc);
	cout << "Ingrese el nuevo correo electronico: "; cin >> act_em;
	cout << "Ingrese el nuevo documento: "; cin >> act_dni;
	cout << "Ingrese la nueva fecha de nacimiento: "; cin >> act_d >> act_m >> act_a;
	p[cual-1].AsignarNombre(act_nom); p[cual-1].AsignarApellido(act_ap);
	p[cual-1].AsignarTelefono(act_tel); p[cual-1].AsignarDireccion(act_direc);
	p[cual-1].AsignarEmail(act_em); p[cual-1].AsignarDni(act_dni); p[cual-1].AsignarFechaNac(act_d,act_m,act_a);
	
	ofstream archivo_actualizado("ListaSocios.txt",ios::trunc);
	for(size_t i=0;i<p.size();i++) { 
		archivo_actualizado << p[i].VerNombre() << endl;
		archivo_actualizado << p[i].VerApellido() << endl;
		archivo_actualizado << p[i].VerTelefono() << endl;
		archivo_actualizado << p[i].VerDireccion() << endl;
		archivo_actualizado << p[i].VerEmail() << endl;
		archivo_actualizado << p[i].VerDni() << endl;
		archivo_actualizado << p[i].VerDiaNac() << endl; 
		archivo_actualizado << p[i].VerMesNac() << endl;
		archivo_actualizado << p[i].VerAnioNac() << endl;
	}
}


void Socios::PruebaVector(){
	
	
	cout << "Tamanio de P consultado desde clase Socios: " << p.size() << endl;
	//	for(int i=0;i<p.size();i++) { 
	//		cout << p[i].VerNombre() << endl;
	//		cout << p[i].VerApellido() << endl;
	//		cout << p[i].VerTelefono() << endl;
	//		cout << p[i].VerDireccion() << endl;
	//		cout << p[i].VerEmail() << endl;
	//		cout << p[i].VerDiaNac() << "/" << p[i].VerMesNac() << "/" << p[i].VerAnioNac();
	//	}
}
void Socios::BuscarNombre(string n){
	
	for(size_t i=0;i<p.size();i++) { 
		if (p[i].VerNombre() == n){
			cout << "Nombre: " << p[i].VerNombre() <<" " << p[i].VerApellido() << endl;
			cout << "   Telefono: " << p[i].VerTelefono() << endl;
			cout << "   Direccion: " << p[i].VerDireccion() << endl;
			cout << "   Correo electronico: " << p[i].VerEmail() << endl;
			cout << "   Documento: " << p[i].VerDni() << endl;
			cout << "   Fecha de nacimiento: " << p[i].VerDiaNac() << "/" << p[i].VerMesNac() << "/" << p[i].VerAnioNac() << endl;
		}
		//else{ cout << "No se encontraron similitudes en la base de datos." << endl; break;}
	}
}


int Socios::CantidadDeSocios(){
	CargarArchivoAVector();
	return p.size();
}


int Socios::DevolverNumeroSocio(int dni){
	for(size_t i=0;i<p.size();i++) { 
		if(p[i].VerDni() == dni){
			return i;
		
		}
	}
	return -1;
}


string Socios::DevolverNombre(int n){
	return p[n].VerNombre();
}


