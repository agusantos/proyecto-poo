#include <fstream>
#include <cstring>
#include "Persona.h"
#include <string>
using namespace std;


void Persona::AsignarNombre(string n){
	m_nombre=n;
}

void Persona::AsignarApellido(string n){
	m_apellido=n;
}
void Persona::AsignarTelefono(long long t){
	m_telefono=t;
}

void Persona::AsignarDireccion(string n){
	m_direccion=n;
}

void Persona::AsignarEmail(string n){
	m_email=n;
}
void Persona::AsignarFechaNac(int a, int b, int c){
	m_dianac = a;  m_mesnac = b;  m_anionac = c;
}

void Persona::AsignarDni(int dni){
	m_dni = dni;
}


string Persona::VerNombre() const{
	return m_nombre;
}

string Persona::VerApellido() const{
	return m_apellido;
}
long long Persona::VerTelefono() const{
	return m_telefono;
}

string Persona::VerDireccion() const{
	return m_direccion;
}

string Persona::VerEmail() const{
	return m_email;
}

int Persona::VerDiaNac() const{
	return m_dianac;
}
int Persona::VerMesNac() const{
	return m_mesnac;
}
int Persona::VerAnioNac() const{
	return m_anionac;
}

int Persona::VerDni() const{
	return m_dni;
}

