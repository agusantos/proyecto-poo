#include "Pagos.h"
#include <iostream>
#include <tuple>
#include <ctime>
using namespace std;

Pagos::Pagos(){
	//CargarVectorPago();
	
	
	
}

void Pagos::Prueba(){
	
	
//	cout << "p: " << p.size() << endl;
//	cout << "v: " << v.size() << endl;
	
 BorrarPagoSocio(4);
//
//		
//	for(int i=0;i<p.size();i++) {
//			cout << v[i+1].s_socio << " " << v[i+1].s_plan << endl;
//			cout <<  v[i+1].ult_dia << " " << v[i+1].ult_mes << " " <<v[i].ult_anio << endl;
//		}
}
void Pagos::Pagar(int socio, int plan){
	
	
	

	int dia,mes,anio;
	tie(dia,mes,anio) = Horario();
	ofstream archivo("Pagos.txt",ios::app);
	archivo  << socio << " " << plan << endl;
	archivo << dia << " " << mes+1 << " " << anio+1900 << endl;
	archivo.close();
	
	
}


void Pagos::CargarVectorPago(){
	CargarArchivoAVector();		/// Pasa el tamaño del vector P a la clase Pagos
	v.resize(p.size()+5);			/// Le asigna al vector v, el tamaño de P
	ifstream archi("Pagos.txt");
	int auxsocio, auxplan, auxdia,auxmes,auxanio;
	while(archi>>auxsocio >> auxplan and archi >> auxdia >> auxmes >> auxanio){
		//cout << "///////" << endl;
		v[auxsocio].s_socio = auxsocio;
		v[auxsocio].s_plan = auxplan;
		v[auxsocio].ult_dia = auxdia;
		v[auxsocio].ult_mes = auxmes;
		v[auxsocio].ult_anio = auxanio;
	}
}	



void Pagos::ConsultarUltimoPago(int socio){
	CargarVectorPago();
	struct tm horaActual;
	time_t ahora;
	ahora=time(NULL);
	horaActual=*localtime(&ahora);
/*	v.resize(p.size()+1);*/
	int diferencia = CalcularDiferencia(socio);
	
	if(socio > v.size()) { cout << "No hay registros de pago del socio indicado. " << endl; }
	else if(v[socio].ult_dia == 0){ cout << "El socio indicado nunca efectuo un pago." << endl; }
	else if(v[socio].ult_dia == horaActual.tm_mday){ cout << "El socio " << socio << " abono por ultima vez hoy. " << endl;}
	else{
		
		cout << "--------------> " << v[socio].s_diferencia << endl;
		if(diferencia == 0){ cout << "No hay registro de pago del socio indicado. " << endl; }
		else{cout << "El socio "<< socio << " abono por ultima vez hace "<< diferencia << " dias." << endl;}
	}
}

int Pagos::CalcularDiferencia(int socio){
	CargarVectorPago();
	struct tm horaActual;
	time_t ahora;
	ahora=time(NULL);
	horaActual=*localtime(&ahora);
	
	struct std::tm actual = {0,0,0,horaActual.tm_mday,horaActual.tm_mon+1,horaActual.tm_year}; /*Fecha actual*/
	struct std::tm ult_pago = {0,0,0,v[socio].ult_dia,v[socio].ult_mes,v[socio].ult_anio-1900}; /* Fecha de pago */
	std::time_t y = std::mktime(&actual);	
	std::time_t x = std::mktime(&ult_pago); double diferencia; 
	diferencia = std::difftime(y, x) / (60 * 60 * 24);
	v[socio].s_diferencia = diferencia;
	
	return diferencia;
}

void Pagos::BorrarPagoSocio(int socio){
	CargarVectorPago();
	
	ofstream archiv_pago("Pagos.txt",ios::trunc);
	
	for(int i=socio;i<p.size();i++) { 
		v[i].s_socio = v[i].s_socio -1;
		v[i].s_plan = v[i-1].s_plan;
		v[i].ult_dia = v[i-1].ult_dia;
		v[i].ult_mes = v[i-1].ult_mes;
		v[i].ult_anio = v[i-1].ult_anio;
	}
	
//		for(int i=0;i<p.size();i++) {
//				cout << v[i+1].s_socio << " " << v[i+1].s_plan << endl;
//				cout <<  v[i+1].ult_dia << " " << v[i+1].ult_mes << " " <<v[i+1].ult_anio << endl;
//			}
	
	v.erase(v.begin()+socio);
	for(int i=0;i<p.size();i++) { 																/// ARREGLAR, NO FUNCIONA.
		archiv_pago << v[i+1].s_socio << " " << v[i+1].s_plan << endl;
		archiv_pago << v[i+1].ult_dia << " " << v[i+1].ult_mes << " " << v[i+1].ult_anio << endl;
	}
}


int Pagos::ConsultarPlan(int socio){
	return v[socio].s_plan;
}

int Pagos::ConsultarDiferencia(int socio){
	return v[socio].s_diferencia;
}
/// Hacer un struct que tenga dia mes y año de ultima vez q abono, y un vector de ese struct.
tuple<int,int,int> Pagos::Horario(){
	
	struct tm horaActual;
	time_t ahora;
	ahora=time(NULL);
	
	horaActual=*localtime(&ahora);
	
	return make_tuple(horaActual.tm_mday, horaActual.tm_mon, horaActual.tm_year);
}



int Pagos::DevolverSocio(int dni){
	int s = DevolverNumeroSocio(dni);
		return s;
}


string Pagos::DevolverNombreIngreso(int socio){
	string nombre = DevolverNombre(socio);
	return nombre;
}
