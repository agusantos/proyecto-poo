#ifndef PERSONA_H
#define PERSONA_H
#include <fstream>
#include <cstring>

#include <string>
using namespace std;

class Persona{
private:
	string m_nombre;
	string m_apellido;
	string m_direccion;
	string m_email;
	int m_dni;
	long long m_telefono;
	int m_dianac,m_mesnac,m_anionac;
	
public:
	void AsignarNombre(string n);
	void AsignarApellido(string n);
	void AsignarTelefono(long long t);
	void AsignarDireccion(string n);
	void AsignarEmail(string n);
	void AsignarFechaNac(int d,int m,int a);
	void AsignarDni(int dni);
	
	string VerNombre() const;
	string VerApellido() const;
	long long VerTelefono() const;
	string VerDireccion() const;
	string VerEmail() const;
	int VerDiaNac() const;
	int VerMesNac() const;
	int VerAnioNac() const;
	int VerDni() const;
	
};
#endif
